from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from .forms import loginForm
from .models import modelLogin
from django.http import HttpResponseRedirect
import google
from google.auth.transport import requests as requests2
from google.oauth2 import id_token
import json
import requests

# Create your views here.
response ={'author':'Hermawan Arifin'}
# def login(request):
#     form = loginForm(request.POST or None)
#     response['form']=form
#     return render(request, 'login.html', response)
def newSubs(request):
    form = loginForm(request.POST or None)
    if(request.method == "POST" and form.is_valid):
        username = request.POST['nama']
        email = request.POST['email']
        password = request.POST['password']
        modelLogin.objects.create(nama=username, email=email, password=password)
        return JsonResponse({'is_success':True})
    else:
        return JsonResponse({'is_success':False})
@csrf_exempt
def chek(request):
    if (request.method == 'POST'):
        email = request.POST['email']
        checkMail = modelLogin.objects.filter(email=email)
        if (checkMail.exists()):
            return JsonResponse({'is_exists':True})
        return JsonResponse({'is_exists':False})

def listSubs(request):
    list_subs = modelLogin.objects.all()
    response['list_subs'] = list_subs
    return render(request, 'listsubscriber.html', response)

def listJson(requests):
    subs = [obj.dictionary() for obj in modelLogin.objects.all()]
    return JsonResponse({"results":subs}, content_type = 'application/json')

@csrf_exempt
def unsubscribe(requests):
    if(requests.method == 'POST'):
        email = requests.POST['email']
        modelLogin.objects.filter(email=email).delete()
        subs = [obj.dictionary() for obj in modelLogin.objects.all()]
        return JsonResponse ({"results" : subs}, content_type = 'application/json')
@csrf_exempt
def login(requests):
    if requests.method == "POST":
        try:
            token = requests.POST['id_token']
            info = id_token.verify_oauth2_token(token, requests2.Request(), "204398802764-c4ato6co8c5vnh2mrph55900rlonid9e.apps.googleusercontent.com")
            if info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise ValueError("Wrong Issuer")
            userId = info['sub']
            email = info['email']
            name = info['name']

            requests.session['user_id'] = userId
            requests.session['email'] = email
            requests.session['name'] = name
            requests.session['book'] = []
            return JsonResponse({"status": "0", 'url':reverse("home")})
        except ValueError:
            return JsonResponse({"status":"1"})
    return render(requests, "login.html")

def logout(requests):
    requests.session.flush()
    return HttpResponseRedirect(reverse('login'))

