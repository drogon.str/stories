from django.db import models

# Create your models here.
class modelLogin(models.Model):
    email = models.EmailField(max_length = 100,unique = True)
    nama = models.CharField(max_length=150)
    password = models.CharField(max_length=150)

    def dictionary(self):
        return{
            "nama":self.nama,
            "email":self.email
        }