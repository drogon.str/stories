from django.test import TestCase, Client
from.views import login

# Create your tests here.
class test_login(TestCase):
    def test_available(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    def test_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "login.html")