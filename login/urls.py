from .views import login, newSubs, chek, listSubs, listJson, unsubscribe, login, logout
from django.urls import path
from landingpage.views import landingPage

urlpatterns = [
    path('newSubs/', newSubs, name="newSubs"),
    path('chek/', chek, name="chek"),
    path('list/', listSubs, name="listSubs"),
    path('listJson/', listJson, name="listJson"),
    path('unsubscribe/', unsubscribe, name="unsubscribe"),
    path("login/", login, name="login"),
    path("home/", landingPage, name="home"),
    path("logout/", logout, name="logout"),
    path('', login, name="login")
]