from django import forms
from .models import modelLogin
class loginForm(forms.Form):
    email = forms.EmailField(max_length=100, required=True, widget=forms.EmailInput(attrs={'id':'email','class':'input100', 'placeholder':'Enter your email'}))
    nama = forms.CharField(max_length=150, required=True, widget=forms.TextInput(attrs={'id':'nama','class':'input100', 'placeholder':'Enter your username'}))
    password = forms.CharField(max_length=150,widget=forms.PasswordInput(attrs={'id':'password','class':'input100', 'placeholder':'Enter your password'}))