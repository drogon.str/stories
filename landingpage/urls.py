from .views import landingPage
from django.urls import path

urlpatterns = [
    path('home/', landingPage),
    path('home/', landingPage, name="home")
]