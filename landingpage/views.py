from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import JsonResponse
import requests

# Create your views here.
def landingPage(request):
    return render(request, "landingpage.html")
