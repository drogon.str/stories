from django.test import TestCase, Client
# Create your tests here.
from .views import landingPage

class test_landing(TestCase):
    def test_available(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 200)
    def test_template(self):
        response = Client().get('/home/')
        self.assertTemplateUsed(response, "landingpage.html")