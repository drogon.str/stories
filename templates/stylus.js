function displayData(text){
    $.ajax({
        method:"GET",
        url: "json_data",
        data: {"search":text},
        success: function(result){
            $('td').remove();
            var book_list = result.data;
            for(i = 0; i < book_list.length; i++){
                var title = book_list[i].title;
                var id = book_list[i].id;
                var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/defstar.png" onclick="addFavorite(id,'+ "'" + text + "'" + ')">'
                var html = '<tr>' +
                             '<td>' + '<img src=' + '"' + book_list[i].image + '">' + '</td>' + 
                             '<td>' + title + '</td>' + 
                             '<td>' + book_list[i].authors + '</td>' + 
                             '<td>' + book_list[i].description + '</td>' + 
                             '<td>' + book_list[i].published + '</td>' + 
                             '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ditemukan")
        }
    });
}

function addFavorite(id,text) {
    $.ajax({
        method:"GET",
        url: "json_data",
        data: {"search":text},
        success: function(result){
            var data = result.data;
            for (i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if (id == id2) {
                    var img = document.getElementById(id);
                    if (img.src.match("/static/defstar.png")) {
                        img.src = "/static/colstar.png";
                        clicks++;
                        document.getElementById("clicks").innerHTML = clicks;
                    } 
                    else {
                        img.src = "/static/defstar.png";
                        clicks--;
                        document.getElementById("clicks").innerHTML = clicks;
                    }
                }
            }
        },
        error: function(error){
            alert("Books not found");
        }
    });
}

