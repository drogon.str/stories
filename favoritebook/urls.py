from .views import bookList
from .views import json_data
from django.urls import path

urlpatterns = [
    path('booklist/', bookList, name = "books"),
    path('json_data/<param>', json_data, name='json_data')
]