from django.test import TestCase, Client
from . views import bookList
# Create your tests here.

class test_Fav(TestCase):
    def test_avail(self):
        response = Client().get('/booklist/')
        self.assertEqual(response.status_code, 200)
    def test_temp(self):
        response = Client().get('/booklist/')
        self.assertTemplateUsed(response, "book.html")
