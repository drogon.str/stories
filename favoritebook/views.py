from django.shortcuts import render
import requests
from django.http import JsonResponse
import urllib.request as urling
import json
# Create your views here.
def bookList(request):
    return render(request, 'book.html')
    
def json_data(request, param="quilting"):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + param
    response = urling.urlopen(url)
    data = json.loads(response.read())
    return JsonResponse(data)

    # search = requests.GET.get('search')
    # if(search == None):
    #     get = requests.get("https://www.googleapis.com/books/v1/volumes?q=quilting"+search).json()
    # else:
    #     get = requests.get("https://www.googleapis.com/books/v1/volumes?q="+search).json()
    # result = get['items']
    # newRes = []
    # for items in result:
    #     data = items['id']
    #     dictionary = {"title": items['volumeInfo']['title'], "authors":items['volumeInfo']['authors'],
    #     "published": items['volumeInfo']['publishedDate'], "image": items['volumeInfo']['imageLinks']['thumbnail'],
    #     "description": items['volumeInfo']['description'], 'id':items['id']}
    #     newRes.append(dictionary)
    # return JsonResponse({'data':newRes})

