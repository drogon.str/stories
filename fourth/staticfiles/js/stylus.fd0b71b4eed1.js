function displayData(text){
    $.ajax({
        url: "/json_data/" + text,
        success: function(result){
            $('td').remove();
            var book_list = result.items;
            for(i = 0; i < book_list.length; i++){
                var volInfo = book_list[i].volumeInfo;
                var title = volInfo.title;
                var id = volInfo.id;
                var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/css/defstar.png" onclick="addFavorite(id,'+ "'" + text + "'" + ')">'
                var html = '<tr>' +
                             '<td>' + title + '</td>' + 
                             '<td>' + volInfo.authors + '</td>' + 
                             '<td>' + volInfo.description + '</td>' + 
                             '<td>' + volInfo.publishedDate + '</td>' + 
                             '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ditemukan")
        }
    });
}

function addFavorite(id,text) {
    $.ajax({
        method:"GET",
        url: "json_data",
        data: {"search":text},
        success: function(result){
            var data = result.data;
            for (i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if (id == id2) {
                    var img = document.getElementById(id);
                    if (img.src.match("/static/css/defstar.png")) {
                        img.src = "/static/css/colstar.png";
                        clicks++;
                        document.getElementById("clicks").innerHTML = clicks;
                    } 
                    else {
                        img.src = "/static/css/defstar.png";
                        clicks--;
                        document.getElementById("clicks").innerHTML = clicks;
                    }
                }
            }
        },
        error: function(error){
            alert("Books not found");
        }
    });
}

