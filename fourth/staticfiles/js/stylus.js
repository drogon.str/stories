function displayData(text){
    $.ajax({
        url: "/json_data/" + text,
        success: function(result){
            $('td').remove();
            var book_list = result.items;
            for(i = 0; i < book_list.length; i++){
                var volInfo = book_list[i].volumeInfo;
                var title = volInfo.title;
                var id = volInfo.id;
                var button = '<img class="fav-btn" id="' + id + '" width="25" height="25" src="/static/css/defstar.png" onclick="addFavorite(id,'+ "'" + text + "'" + ')">'
                var html = '<tr>' +
                             '<td>' + title + '</td>' + 
                             '<td>' + volInfo.authors + '</td>' + 
                             '<td>' + volInfo.description + '</td>' + 
                             '<td>' + volInfo.publishedDate + '</td>' + 
                             '<td>' + button + '</td>' + '</tr>';
                $('tbody').append(html);
            }
        },
        error: function(error){
            alert("Buku tidak ditemukan")
        }
    });
}

function addFavorite(id,text) {
    $.ajax({
        method:"GET",
        url: "json_data",
        data: {"search":text},
        success: function(result){
            var data = result.data;
            for (i = 0; i < data.length; i++) {
                var id2 = data[i].id;
                if (id == id2) {
                    var img = document.getElementById(id);
                    if (img.src.match("/static/css/defstar.png")) {
                        img.src = "/static/css/colstar.png";
                        clicks++;
                        document.getElementById("clicks").innerHTML = clicks;
                    } 
                    else {
                        img.src = "/static/css/defstar.png";
                        clicks--;
                        document.getElementById("clicks").innerHTML = clicks;
                    }
                }
            }
        },
        error: function(error){
            alert("Books not found");
        }
    });
}

$(function(){
    var csrftoken = $("[name=csrfmiddlewaretoken]").val();
    var available;
    var timer = 0;

    $('#email').keydown(function(){
        clearTimeout(timer)
        timer = setTimeout(checkValid, 500);
        warning();
    });

    $('#nama').keydown(function(){
        clearTimeout(timer);
        timer = setTimeout(warning, 500);
    });

    $('#password').keydown(function(){
        clearTimeout(timer);
        timer = setTimeout(warning, 500);
    });

    $('#form').on('submit', function(event){
        console.log("kesubmit ea");
        event.preventDefault();
        saveForm();
    });
    function saveForm(){
        $.ajax({
            method: 'POST',
            url: "../newSubs/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                nama: $('#nama').val(),
                email: $('#email').val(),
                password: $('#password').val(),
            },
            success: function (response) {
                console.log("hcfxgnbc");
                if (response.is_success) {
                    $('#nama').val('');
                    $('#email').val('');
                    $('#password').val('');
                    $('.login100-form-btn').prop('disabled', true);
                    $('.error p').replaceWith("<p class='success'>Berhasil subscribe!</p>");
                    console.log("Successfully add data");
                } else {
                    $('.error p').replaceWith("<p class='fail'>Oops!</p>");
                }
            },
            error: function () {
                alert("Error, cannot save data to database");
            }
        })

    }
    function checkValid(){
        var val = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var is_valid = val.test($('#email').val());
        if (is_valid) {
            checkEmail();
        } else {
            $('.error p').replaceWith("<p class='fail'>Masukkan email dengan benar!</p>");
        }

    }


    function checkEmail(){
        $.ajax({
            method: 'POST',
            url: "../chek/",
            headers: {
                "X-CSRFToken": csrftoken,
            },
            data: {
                email: $('#email').val(),
            },
            success: function (email) {
                if (email.is_exists) {
                    available = false;
                    $('.error p').replaceWith("<p class='fail'>Email sudah pernah digunakan!</p>");
                } else {
                    available = true;
                    warning();
                }
            },
            error: function () {
                alert("Error, tidak bisa memvalidasi email")
            }
        })

    }

    function warning(){
        var nama = $('#nama').val()
        var email = $('#email').val()
        var password = $('#password').val()

        if (password.length !== 0 && nama.length !== 0 && available) {
            $('.error p').replaceWith("<p></p>");
            $('.login100-form-btn').prop('disabled', false);
        } 
        else if (password.length === 0 && nama.length === 0) {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Nama dan password tidak boleh kosong</p>");
        } 
        else if (password.length === 0 && email.length === 0) {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Email dan password tidak boleh kosong</p>");
        } 
        else if (nama.length === 0 && email.length) {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Nama dan email tidak boleh kosong</p>");
        } 
        else if (password.length === 0) {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Password tidak boleh kosong</p>");
        } 
        else if (password.length < 6) {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Password harus berisi setidaknya 6 karakter</p>");
        } 
        else if (nama.length === 0) {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Nama tidak boleh kosong</p>");
        } 
        else if (email.length === 0) {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Email tidak boleh kosong</p>");
        } 
        else {
            $('.login100-form-btn').prop('disabled', true);
            $('.error p').replaceWith("<p class='fail'>Masukkan email dengan benar</p>");
        }

    }
});
